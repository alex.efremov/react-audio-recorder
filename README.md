Audio Recorder component for React

To-Do: 
    1. Replace state in Oscilloscope with class property.
    2. Implement shouldComponentUpdate in AudioRecorder and eliminate condition inside didComponentUpdate method.
    3. Implement a handling of situation when a media stream is stopped but shouldVisualize is true.
    4. Implement a handling of situatiion when visualizing is in action and the media stream is stoping.