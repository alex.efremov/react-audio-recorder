import AudioRecorder from './elements/AudioRecorder';
import Oscilloscope from './elements/Oscilloscope';

module.exports = {
  AudioRecorder,
  Oscilloscope
};
